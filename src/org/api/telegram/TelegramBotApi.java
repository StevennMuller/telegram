package org.api.telegram;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;



public class TelegramBotApi {

	/* DOCUMENTAÇÃO
	 * http://www.mkyong.com/java/apache-httpclient-examples/
	 */
	
	public static  String processCommand(String command, Map<String,String> params) throws UnsupportedOperationException, IOException, JSONException{
		//get
		String url = "https://api.telegram.org/bot"+params.get("token")+"/"+command;

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);

		// add request header
		HttpResponse response = client.execute(request);

		System.out.println("Response Code : " 
	                + response.getStatusLine().getStatusCode());
				BufferedReader rd = new BufferedReader(
			new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		System.out.println(result);
		return result.toString();
	}
	
}
