package telegram.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

public class TelegramBotApi {
	
	  /**
	   * Executa um comando do telegram	
	   * @param command comando a ser executado pela API para envio de uma mensagem, video ou outra a��o.
	   * @param params  par�metros suportados pelo comando escolhido no par�metro anterior.
	   * @return
	   */
	  public static Map<String,String> processCommand(String command, Map<String,String> params){
		  
		  	//Forma��o da primeira parte da url, passando o comando escolhido
		  	String url = "https://api.telegram.org/bot" + params.get("token") + "/" + command + "?";
		  	
		  	//Passagem dos par�metros do mapa para a url
		  	Object value = "";
		    for (String key : params.keySet()) {   
		    	value = params.get(key);
		    	url = url + key + "=" + value + "&";
            }  
		    url= url.substring(0, url.length()-1); //aqui eu retiro o "&" no final da String, n�o sei se tem uma 
		                                           //forma melhor de fazer
		  	
		  		
			    //Inializa��o de objetos para execu��o da requisi��o
		    	
		        HttpClient client =  new DefaultHttpClient();
		        client = WebClientDevWrapper.wrapClient(client);
			 	HttpUriRequest request = new HttpGet(url);

			    try{
					
					//Execu��o da requisi��o
			    	HttpResponse response = client.execute(request);
			    	//System.out.println("Resposta da requisi��o: " + response.toString());
			    	
			    	//Porcessamento da resposta	
			    	BufferedReader breader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			    	StringBuilder responseString = new StringBuilder();
			    	String line = "";
			    	while ((line = breader.readLine()) != null) {
			    		responseString.append(line);
			    	}
			    	breader.close();
			    	String responseStr = responseString.toString();
			    	
			    	//Cria��o do mapa com o resultado da requisi��o
			    	Map<String,String>  result = new HashMap<String, String>();
			    	result = jsonToMap(responseStr);
			    	return result;
			    }
			    
			    catch (IOException e){
			      e.printStackTrace();
			      return null;
			    }
	  }
	  
	  /**
	   * Converte o json de resposta da requisi��o para um mapa
	   * @param jsonResult json que ser� convertido em mapa.
	   * @return           mapa com os dados retornados pela requisi��o.      
	   */
	  public static Map<String,String> jsonToMap(String jsonResult){
		  Map<String,String> result =  new HashMap<String, String>();
		  result.put("result", jsonResult);
		  return result;
	  }
}
